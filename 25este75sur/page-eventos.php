<?php
/*
Template Name: Eventos
*/
?>

<?php get_header(); ?>
<div id="subtittle" class="box subtitle .style_1 .article "><a href="<?php echo bloginfo('url') ?>/category/evento" tittle="Vamos a HOME!">eventos de esta semana...</a></div>
<?php
  get_template_part( 'loop', 'eventsActual' );
?>
<div id="subtittle" class="box subtitle .style_1 .article "><a href="<?php echo bloginfo('url') ?>/category/evento" tittle="Vamos a HOME!">eventos pasados...</a></div>
<?php
  get_template_part( 'loop', 'eventsPast' );
?>
<?php get_footer(); ?>
